function randomNumGen() {
  return Math.floor(Math.random() * 100);
}
export function tableDataGen(n) {
  const data = [];
  for(let i = 0; i < n; i++) {
    data.push({
      "col1": randomNumGen(),
      "col2": randomNumGen(),
      "col3": randomNumGen(),
      "col4": randomNumGen(),
    })
  }
  return data;
}
