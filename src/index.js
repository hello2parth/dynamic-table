import m from "mithril";
import { tableDataGen } from './tableData.js';
import '!style-loader!css-loader!tachyons/css/tachyons.css';
import '!style-loader!css-loader!./styles.css';
import nextImg from './icons/next.png';
import nextPageImg from './icons/nextPage.png';
import lastPageImg from './icons/lastPage.png';
import backImg from './icons/back.png';
import backPageImg from './icons/backPage.png';
import path from"path";

const initialIndex = 1;

let data = tableDataGen(1000000); // initial no. of rows assumption
const oConfig = {
  showing: 1000,
  totalRows: 1000000,
  start: initialIndex,
  getShowing: function() {
    return parseInt(oConfig.showing, 10);
  },
  getTotalRows: function() {
    return parseInt(oConfig.totalRows, 10) + initialIndex;
  },
  getStart: function() {
    return parseInt(oConfig.start, 10);
  }
}

function numValidation(s) {
  return Number(s.replace(/[\D]*/g,''));
}

function generateRows() {
  return {
    view: function(vnode) {
      let { start, showing, totalRows } = vnode.attrs.oConfig;
      let tableData = vnode.attrs.data;
      let rows = [];
      start = parseInt(start, 10);
      showing = parseInt(showing, 10);
      let loopLength = start + showing;
      if (!!totalRows) {
        for (let i = start; i < loopLength; i++) {
          let row = tableData[i - initialIndex];
          if (!!row) {
            rows.push((m("tr.striped--light-gray", {key: i}, [
              m("td.pv2.ph2.tc", {key: `sr-${i}`}, i),
              m("td.pv2.ph2.tc", {key: `col1-${i}`}, row['col1']),
              m("td.pv2.ph2.tc", {key: `col2-${i}`}, row['col2']),
              m("td.pv2.ph2.tc", {key: `col3-${i}`}, row['col3']),
              m("td.pv2.ph2.tc", {key: `col4-${i}`}, row['col4'])
            ])))
          }
        }
      }
      return rows;
    }
  };
}

let App = {
  view: function(vnode) {
    return(
      m("div.app", [
        m("div.header.w-100.flex.justify-center.items-center.bb.b--black-10.bg-moon-gray.fixed", [
          m("span.ml2.w4s.pointer", {
            title: 'First Page',
            onclick: function() {
              oConfig.start = initialIndex;
            }
          }, m("img.h1.firstPage", {src: path.join("bin", lastPageImg)})),
          m("span.ml2.w4s.pointer", {
            title: 'Previous Page',
            onclick: function() {
              if ((oConfig.getStart() - oConfig.getShowing()) > 1) {
                oConfig.start = oConfig.getStart() - oConfig.getShowing();
              } else {
                oConfig.start = initialIndex;
              }
            }
          }, m("img.h1", {src: path.join("bin", backPageImg)})),
          m("span.ml2.w4s.pointer", {
            title: 'Previous',
            onclick: function() {
              if (oConfig.getStart() > initialIndex) {
                oConfig.start = oConfig.getStart() - 1;
              }
            }
          }, m("img.h1", {src: path.join("bin", backImg)})),
          m("label.ml3", "Showing"),
          m("input.ml1.w3", {
            value: oConfig.showing,
            oninput: function (e) {
              let input = numValidation(e.target.value);
              input = input >= 10000 ? 10000 : input; // max row assumption
              oConfig.showing = input <= oConfig.getTotalRows() - initialIndex ? input : oConfig.getTotalRows() - initialIndex; // showing shouldn't exceed total rows
            }}),
          m("label.ml4", "Rows out of"),
          m("input.ml1.w3", {
            value: oConfig.totalRows,
            oninput: function (e) {
              oConfig.totalRows = numValidation(e.target.value);
              if (oConfig.getTotalRows() - initialIndex < oConfig.getShowing()) {
                oConfig.showing = oConfig.getTotalRows() - initialIndex;
              }
              if (oConfig.getStart() > oConfig.getTotalRows()) {
                oConfig.start = initialIndex;
              }
              data = tableDataGen(oConfig.getTotalRows() || 1); // new table data generation
            }}),
          m("label.ml4", "Starting at row"),
          m("input.ml1.w3", {
            value: oConfig.start,
            oninput: function (e) {
              let input = numValidation(e.target.value);
              oConfig.start = input <= oConfig.getTotalRows() - initialIndex ? input : oConfig.getTotalRows() - initialIndex; // start row shouldn't exceed total rows
              if ((oConfig.getStart() + oConfig.getShowing()) > oConfig.getTotalRows()) {
                oConfig.showing = oConfig.getTotalRows() - oConfig.getStart();
              }
            }}),
          m("span.ml3.w4s.pointer", {
            title: 'Next',
            onclick() {
              if (oConfig.getStart() < oConfig.getTotalRows()) {
                oConfig.start = oConfig.getStart() + 1;
              }
            }
          }, m("img.h1", {src: path.join("bin", nextImg)})),
          m("span.ml2.w4s.pointer", {
            title: 'Next Page',
            onclick: function() {
              if ((oConfig.getStart() + oConfig.getShowing()) < oConfig.getTotalRows()) {
                oConfig.start = oConfig.getStart() + oConfig.getShowing();
              }
            }
          }, m("img.h1", {src: path.join("bin", nextPageImg)})),
          m("span.ml2.w4s.pointer", {
            title: 'Last Page',
            onclick: function() {
              oConfig.start = oConfig.getTotalRows() - oConfig.getShowing();
            }
          }, m("img.h1", {src: path.join("bin", lastPageImg)})),
        ]),
        m("div.main.flex.justify-center", [
          m("table",[
            m("thead", [
              m("tr",[
                m("th.pv2.ph2.tc.bg-moon-gray", "Sr."),
                m("th.pv2.ph2.tc.bg-moon-gray", "col-1"),
                m("th.pv2.ph2.tc.bg-moon-gray", "col-2"),
                m("th.pv2.ph2.tc.bg-moon-gray", "col-3"),
                m("th.pv2.ph2.tc.bg-moon-gray", "col-4")
              ])
            ]),
            m("tbody.bg-white", m(generateRows, {oConfig, data}))
          ])
        ]),
      ])
    );
  }
}

m.mount(document.body, App);
